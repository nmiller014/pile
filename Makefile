EXEC_NAME=pile

CC=gcc

CFLAGS=-c -std=gnu99 -O1 -Wall

SOURCES=src/test.c src/pile.c
HEADERS=src/pile.h src/pile_private.h

# generated variables
OBJECTS=$(SOURCES:src/%.c=build/%.o)

ifdef DEBUG
CFLAGS+= -DDEBUG_PRINT
endif

# build rules

all: build/$(EXEC_NAME)

build/$(EXEC_NAME): $(OBJECTS)
	$(CC) $+ -o $@

build/%.o : src/%.c $(HEADERS) | build
	$(CC) $(CFLAGS) $< -o $@

build:
	mkdir -p build

# other rules
	
clean:
	rm -rf build/

.PHONY: all clean