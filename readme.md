# Pile

Pile is an extremely simple dynamic memory allocation system. Theoretically,
you could use it on embedded systems to add dynamic memory allocation, but I
wouldn't recommend it.

Things Pile is not:
* fast
* efficient
* well written
* good

Things pile is:
* Somewhat functional

I basically wrote this for grins, to figure out if I could. It ended up being
simpler than I expected, although it still has a fair number of bugs.