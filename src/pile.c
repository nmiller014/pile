#include <string.h>
#include <stdint.h>
#include <stddef.h>
#include <stdio.h>

#include "pile.h"
#include "pile_private.h"

#ifdef DEBUG_PRINT
#define debug_printf(...) printf(__VA_ARGS__)
#else
#define debug_printf(...)
#endif

static uint8_t heap[PILE_HEAP_SIZE_BYTES];
static PileBlock *firstBlock = NULL;
static void * const heapEnd = (void*)(&heap[PILE_HEAP_SIZE_BYTES]);

static inline PileBlock * getBlock(void *ptr)
{
  return (PileBlock*)((uint8_t*)ptr - sizeof(PileBlock));
}

static PileBlock * findFirstFreeSpace(size_t size)
{
  debug_printf("\n");
  size_t actualSize = size + sizeof(PileBlock);

  debug_printf("requested size = %zu, actual required size = %zu\n",size, actualSize);

  if (size > (PILE_SIZE_T_MAX - sizeof(PileBlock)) || actualSize > PILE_HEAP_SIZE_BYTES)
  {
    debug_printf("Not enough space\n");
    return NULL;
  }

  if (firstBlock == NULL)
  {
    debug_printf("Initializing the first block\n");
    // fill the first block
    firstBlock = (PileBlock*)(&heap[0]);
    firstBlock->size = size;
    firstBlock->prev = NULL;
    firstBlock->next = NULL;

    return (void*)firstBlock;
  }
  else
  {
    debug_printf("There's a non-zero number of blocks, searching...\n");
    // need to hunt for free space

    // If the free space between the current block's data and the following
    // block (or the end of the heap if the next ptr is null) is large enough,
    // we can use that. Otherwise move to the next block

    PileBlock *currentBlock = firstBlock;
    void *firstFollowingCell = NULL;
    void *nextBlock = NULL;
    
    do
    {
      firstFollowingCell = (void*)currentBlock + sizeof(PileBlock) + currentBlock->size;
      nextBlock = currentBlock->next;
      if (nextBlock == NULL)
      {
        debug_printf("I'm looking at the last block\n");
        nextBlock = heapEnd;
      }

      debug_printf("avaiable size between blocks X and Y: %zu\n", (size_t)(nextBlock - firstFollowingCell));

      if ((size_t)(nextBlock - firstFollowingCell) >= actualSize)
      {
        debug_printf("Found enough free space, creating a new block\n");
        // create new block in place and re-link
        PileBlock *newBlock = (PileBlock*)firstFollowingCell;
        newBlock->size = size;
        newBlock->prev = currentBlock;
        newBlock->next = currentBlock->next;
        currentBlock->next = newBlock;

        return newBlock;
      }

      debug_printf("I didn't find a suitable space on that pass\n");

      currentBlock = currentBlock->next;

    } while (currentBlock != NULL);
  }

  return NULL;
}

void* pile_malloc(size_t size)
{
  PileBlock *block = findFirstFreeSpace(size);

  if (block != NULL)
  {
    void *data = (void*)(block + 1);
    return data;
  }

  return NULL;
}

void* pile_calloc(size_t num, size_t size)
{
  void *data = pile_malloc(num * size);
  if (data != NULL)
  {
    memset(data, 0, num * size);
  }

  return data;
}

void* pile_realloc(void *ptr, size_t size)
{
  if (ptr == NULL)
  {
    // if the ptr is null, just malloc and return
    return pile_malloc(size);;
  }
  else
  {
    PileBlock *block = getBlock(ptr);
    if (size < block->size)
    {
      // we can just reduce the block size and move on
      block->size = size;
    }
    else if (size > block->size)
    {
      // is there enough space to simply expand the block?
      uint8_t *nextUsedCell;
      if (block->next == NULL)
      {
        // next used cell is the end of the heap
        nextUsedCell = heapEnd;
      }
      else
      {
        // next used cell is the next block
        nextUsedCell = (uint8_t*)(block->next);
      }
      
      if ((ptrdiff_t)(nextUsedCell - (uint8_t*)ptr) >= (ptrdiff_t)size)
      {
        //  if so, update the size and move on
        block->size = size;
      }
      else
      {
        //  otherwise,
        //    malloc a new block
        //    copy the original data
        //    free the old block
        void *newPtr = pile_malloc(size);
        if (newPtr != NULL)
        {
          memcpy(newPtr, ptr, block->size);
          pile_free(ptr);
        }
        return newPtr;
      } 
    }
  }

  return ptr;
}

void pile_free(void *ptr)
{
  debug_printf("\nFreeing memory at %p\n", ptr);
  PileBlock *block = getBlock(ptr);
  debug_printf("block addr =      %p\n", block);
  PileBlock *prev = block->prev;
  PileBlock *next = block->next;

  // if this is the first block, update firstBlock to be its next. This still
  // works out if there is no next, because it means this was the only block
  if (block == firstBlock)
  {
    debug_printf("Freeing the first block\n");
    firstBlock = next;
  }

  if (next != NULL)
  {
    debug_printf("This is NOT the last block\n");
    next->prev = prev;
  }

  if (prev != NULL)
  {
    debug_printf("This is NOT the first block\n");
    prev->next = next;
  }
}