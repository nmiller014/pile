#ifndef PILE_H
#define PILE_H

#include <stddef.h>

#ifndef PILE_HEAP_SIZE_BYTES
#define PILE_HEAP_SIZE_BYTES (256)
#endif

void* pile_malloc(size_t size);
void* pile_calloc(size_t num, size_t size);
void* pile_realloc(void* ptr, size_t size);
void pile_free(void* ptr);

#endif