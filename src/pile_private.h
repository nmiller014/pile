#ifndef PILE_PRIVATE_H
#define PILE_PRIVATE_H

typedef struct PileBlock
{
  struct PileBlock *prev;
  struct PileBlock *next;
  uint16_t size;
} PileBlock;

#define PILE_SIZE_T_MAX ((size_t)(-1))

#define MAX_BLOCK_COUNT (PILE_HEAP_SIZE_BYTES / (sizeof(PileBlock) + 1))

#endif