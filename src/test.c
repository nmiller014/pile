#include <assert.h>
#include <stdio.h>
#include <stdint.h>

#include "pile.h"
// implementing applications should not include pile_private.h,
// I just included it here so that I could sizeof(PileBlock)
#include "pile_private.h"

int main(void)
{
  printf("Running pile tests...\n");
  uint8_t i;

  // test malloc by making sure we get a useable pointer
  uint8_t *ptr1 = pile_malloc(sizeof(uint8_t));
  assert(ptr1 != NULL);
  *ptr1 = 5;
  assert(*ptr1 == 5);
  *ptr1 = 10;
  assert(*ptr1 == 10);
  
  // test a second malloc and verify that both pointers are still independant
  uint16_t *ptr2 = pile_malloc(sizeof(uint16_t));
  assert(ptr2 != NULL);
  *ptr2 = 25;
  assert(*ptr2 == 25);
  *ptr2 = 20;
  assert(*ptr2 == 20);

  *ptr1 = 30;
  assert(*ptr1 == 30);
  assert(*ptr2 == 20);

  // test a malloc that should fail due to lack of space
  uint8_t *ptr3 = pile_malloc(PILE_HEAP_SIZE_BYTES - sizeof(PileBlock));
  assert(ptr3 == NULL);
  
  // test free by freeing space and trying the large malloc again
  pile_free(ptr1);
  pile_free(ptr2);

  uint8_t *ptr4 = pile_malloc(PILE_HEAP_SIZE_BYTES - sizeof(PileBlock));
  assert(ptr4 != NULL);
  
  // calloc should fail due to lack of space
  uint8_t *ptr5 = pile_calloc(1, sizeof(uint8_t));
  assert(ptr5 == NULL);

  pile_free(ptr4);

  // calloc should succeed and initialize the pointer data to 0
  uint8_t *ptr6 = pile_calloc(1, sizeof(uint8_t));
  assert(ptr6 != NULL);
  assert(*ptr6 == 0);

  // calloc should succeed and initialize ALL of the pointer data to 0
  uint8_t *ptr7 = pile_calloc(10, sizeof(uint8_t));
  assert(ptr7 != NULL);

  for (i = 0; i < 10; i++)
  {
    assert(*(ptr7 + i) == 0);
  }

  pile_free(ptr6);
  pile_free(ptr7);

  // SO MANY CASES FOR REALLOC

  // realloc a null pointer, should just malloc new data
  // malloc something in between
  // realloc to a smaller size, and try to malloc in between
  // free the previous malloc, try to resize to larger size


  printf("Tests finished successfully\n");
  return 0;
}